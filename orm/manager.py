from sqlalchemy import update
from sqlalchemy import Table, MetaData
from sqlalchemy.sql import text

try:
    from .models import Station, Protocol, DBData, DBType
except Exception:
    from models import Station, Protocol, DBData, DBType

try:
    from .db_session import session, engine, connection
except Exception:
    from db_session import session, engine, connection

import datetime

from geoalchemy2 import functions as geo_func

from sqlalchemy.sql import select
from sqlalchemy.sql import table
from sqlalchemy.sql import column
from sqlalchemy import inspect

def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


class SessionHandle(object):
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_ and obtain
    lists from tables
    """

    def __init__(self):
        self.session = session
        self.conn = connection
        self.metadata = MetaData()

    def close(self):
        self.session.close()

    def exists_table(self, table_name):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        return engine.dialect.has_table(engine.connect(), table_name)

    def exists_field(self, table_name, field_name):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError:
            pass
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value):
        """
        Check if value is the same value type in field
        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name, field_name), \
            'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[0:7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

        def generic_query(self, st):
            q = text(st)
            u = self.session.execute(q)
            return(u)

    def update_table(self, table_name, instance, field_name, value):
        """
        Change some value in table
        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :return: void()
        """
        # update to database
        table = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        # print(table+":"+field_name+":"+value)
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        print(up)
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :return: a query list
        """
        return self.session.query(model).all()

# Class Alias
SH = SessionHandle


class SessionCollector(SH):
    """
    An specific SessionHandler who extends database model in Collector case,
    has a Station, a Protocol and a FBData tables
    """

    # STATION TABLE

    def station(self,
                **kwargs):
        if bool(kwargs):
            code=kwargs['code']
            name=kwargs['name']
            position=kwargs['position']
            host=kwargs['host']
            port=int(kwargs['port'])
            interface_port=int(kwargs['interface_port'])
            db=kwargs['db']
            protocol=kwargs['protocol']
        lat = position[0]
        lon = position[1]
        instance=self.session.query(Station).filter_by(code=code).first()
        if instance:
            return instance
        else:
            print("Check interface port %s" % type(interface_port))
            point = "POINT(" + str(lat) + " " + str(lon) + ")"
            # check if db and protocol exists
            db_e=object
            prot_e=object
            if isinstance(db, int):
                db_e = self.get_dbdata_by_id(db)
                db_id = db_e.id
            else:
                db_e = self.get_dbdata_id(db)
                db_id = db_e.id
            if isinstance(protocol, int):
                prot_e = self.get_protocol_by_id(protocol)
                prot_id=prot_e.id
            else:
                prot_e = self.get_protocol_id(protocol)
                prot_id=prot_e.id

            print(db_e)
            print(prot_e)
            print("Check interface port again %s" % type(interface_port))
            if db_e and prot_e:
                station = Station(code=code,
                                  name=name,
                                  position=point,
                                  interface_port=interface_port,
                                  port=port,
                                  host=host,
                                  db_id=db_id,
                                  protocol_id=prot_id)
                self.create_station(station)
                return station
            else:
                return None

    def create_station(self, station):
        self.session.add(station)
        self.session.commit()

    def update_station(self, instance, new_dict):
        t_name = 'station'
        station= object_as_dict(instance)
        db=new_dict['db']
        protocol=new_dict['protocol']
        db_e=object
        prot_e=object
        if isinstance(db, int):
            db_e = self.get_dbdata_by_id(db)
            db_id = db_e.id
        else:
            db_e = self.get_dbdata_id(db)
            db_id = db_e.id
        if isinstance(protocol, int):
            prot_e = self.get_protocol_by_id(protocol)
            prot_id=prot_e.id
        else:
            prot_e = self.get_protocol_id(protocol)
            prot_id=prot_e.id
        new_dict['protocol_id']=prot_id
        new_dict['db_id']=db_id
        for k in new_dict.keys():
            if k in station.keys():
                v = new_dict[k]
                if type(v)==type(station[k]) and v!=station[k]:
                    self.update_table(t_name, instance, k, v)

    def delete_station(self, station):
        self.session.delete(station)
        self.session.flush()

    def get_stations(self):
        stmt=select([column('id'),
                     column('code'),
                     column('name'),
                     geo_func.ST_AsGeoJSON(column('position')),
                     column('port'),
                     column('interface_port'),
                     column('host'),
                     column('db_id'),
                     column('protocol_id')]).select_from(table('station'))

        stations=self.get_list(Station)
        #for sta in stations:
        new_stations=stations
        return new_stations

    def get_station_id(self, code):
        # code is a unique value
        u = self.session.query(Station).filter_by(code=code).all()
        if len(u) > 0:
            return u[0].id
        else:
            return None

    def get_station_by_id(self, pk):
        u = self.session.query(Station).filter_by(id=pk).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_stations_near(self, center_point, km):
        lat = center_point[0]
        lon = center_point[1]
        point = "POINT(" + str(lon) + " " + str(lat) + ")"
        Q = "ST_GeogFromText(\'%s\')" % point
        print(point)
        # u = self.session.query(Station).filter(
        #    functions.ST_DWithin(
        #        Station.position,
        #        Q, km * 1000,true))
        strq = "select * from station where ST_DWithin(ST_GeogFromText(\'" + \
            point + "\'),position," + str(km * 1000) + ",true)"
        stmt = text(strq)
        result = self.session.execute(stmt)
        return result

    def get_station_data(self):
        strq = "select station.code as st_code,station.name as st_name,station.host as st_host,station.port as st_port, ST_AsGeoJSON(station.position) as position,protocol.name as prt_name, dbselect.typedb as db_type, dbselect.name as db_name,dbselect.code as db_code, dbselect.path as db_path,dbselect.host as db_host,dbselect.port as db_port,dbselect.user as db_user,dbselect.passw as db_passw,dbselect.info as db_info,dbselect.typedb,dbselect.data_list as db_list FROM station INNER JOIN protocol ON station.protocol_id=protocol.id INNER JOIN (select dbdata.id, dbdata.code, dbdata.path,dbdata.host,dbdata.port,dbdata.user,dbdata.passw,dbdata.info,dbtype.typedb, dbtype.name,dbtype.data_list from dbdata inner join dbtype on dbdata.dbtype_id=dbtype.id) as dbselect on station.db_id=dbselect.id"
        stmt = text(strq)
        result = self.session.execute(stmt)
        return result

    # PROTOCOL TABLE

    def protocol(self, **kwargs):
        if bool(kwargs):
            name=kwargs['name']
            ref=kwargs['ref']
            class_name=kwargs['class_name']
            git=kwargs['git']
        protocol=self.session.query(Protocol).filter_by(name=name).first()
        if protocol:
            return protocol
        else:
            protocol = Protocol(name=name,
                                ref_url=ref,
                                class_name=class_name,
                                git_url=git)
            self.create_protocol(protocol)
            return protocol

    def create_protocol(self, protocol):
        self.session.add(protocol)
        self.session.commit()

    def update_protocol(self, instance, fields, values):
        t_name = 'protocol'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def delete_protocol(self, protocol):
        self.session.delete(protocol)
        self.session.flush()

    def get_protocol(self):
        return self.get_list(Protocol)

    def get_protocol_id(self, name):
        # code is a unique value
        u = self.session.query(Protocol).filter_by(name=name).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_protocol_by_id(self, pk):
        u = self.session.query(Protocol).filter_by(id=pk).all()
        if len(u) > 0:
            return u[0]
        else:
            return None
    # DBDATA TABLE

    def dbdata(self, **kwargs):
        if bool(kwargs):
            code = kwargs['code']
            path = kwargs['path']
            host = kwargs['host']
            port = int(kwargs['port'])
            user = kwargs['user']
            passw = kwargs['passw']
            info = kwargs['info']
            if 'name' in kwargs:
                dbtype = kwargs['name']
            else:
                dbtype=kwargs['dbtype']
        instance=self.session.query(DBData).filter_by(code=code).first()
        if instance:
            return instance
        else:
            print(dbtype)
            db_e = self.get_dbtype_id(dbtype)
            if db_e:
                instance = DBData(code=code,
                                  path=path,
                                  host=host,
                                  port=port,
                                  user=user,
                                  passw=passw,
                                  info=info,
                                  dbtype_id=db_e.id)
                self.create_dbdata(instance)
                return instance
            else:
                return None

    def create_dbdata(self, dbdata):
        self.session.add(dbdata)
        self.session.commit()

    def update_dbdata(self, instance, new_dict):
        t_name = 'dbdata'
        dbdata =object_as_dict(instance)
        for k in new_dict.keys():
            if k in dbdata.keys():
                v = new_dict[k]
                db_e = self.get_dbtype_by_id(dbdata)
                new_dict['dbtype_id']=db_e.id
                if type(v)==type(dbdata[k]) and v!=dbdata[k]:
                    self.update_table(t_name, instance, k, v)

    def delete_dbdata(self, dbdata):
        self.session.delete(dbdata)
        self.session.flush()

    def get_dbdatas(self):
        return self.get_list(DBData)

    def get_dbdata_id(self, code):
        # code is a unique value
        u = self.session.query(DBData).filter_by(code=code).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_dbdata_list_by_type(self, dtype):
        # code is a unique value
        u = self.session.query(DBData).filter_by(dbtype=dtype).all()
        return u

    def get_dbdata_by_id(self, pk):
        u = self.session.query(DBData).filter_by(id=pk).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_dbdata_data(self):
        strq = "select * from dbdata inner join dbtype on dbtype_id=dbtype.id"
        stmt = text(strq)
        result = self.session.execute(stmt)
        return result
        pass
    # DBTYPE TABLE

    def dbtype(self, **kwargs):
        if bool(kwargs):
            typedb = kwargs['typedb']
            name = kwargs['name']
            url = kwargs['url']
            data_list = kwargs['data_list']
        instance = self.session.query(DBType).filter_by(name=name).first()
        if instance:
            return instance
        else:
            instance = DBType(
                typedb=typedb,
                name=name,
                url=url,
                data_list=data_list)
            self.create_dbtype(instance)
            return instance

    def create_dbtype(self, dbtype):
        self.session.add(dbtype)
        self.session.commit()

    def update_dbtype(self, instance, fields, values):
        t_name = 'dbtype'
        assert len(fields) == len(values)
        for f in fields:
            v = values[fields.index(f)]
            self.update_table(t_name, instance, f, v)

    def delete_dbtype(self, dbtype):
        self.session.delete(dbtype)
        self.session.flush()

    def get_dbtype(self):
        return self.get_list(DBType)

    def get_dbtype_id(self, name):
        # code is a unique value
        u = self.session.query(DBType).filter_by(name=name).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_dbtype_by_id(self, db_id):
        # code is a unique value
        u = self.session.query(DBType).filter_by(id=db_id).all()
        if len(u) > 0:
            return u[0]
        else:
            return None

    def get_dbtype_list_by_type(self, dtype):
        # code is a unique value
        u = self.session.query(DBData).filter_by(typedb=dtype).all()
        return u
