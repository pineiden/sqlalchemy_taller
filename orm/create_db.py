from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

from collector.orm.models import Base

user='collector'
passw='XdataX'
dbname='collector'
db_engine='postgresql://'+user+':'+passw+'@localhost/'+dbname
#create engine

engine = create_engine(db_engine, echo=True)

try:
    engine.execute(CreateSchema('collector'))
except Exception as exec:
    print(exec)
    raise exec
#load schema on engine
try:
    Base.metadata.create_all(engine, checkfirst=True)
except Exception as exec:
    print(exec)
    raise exec
