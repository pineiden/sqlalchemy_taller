from manager import SessionCollector
from pathlib import Path
import csv

pwd = str(Path(__file__).resolve().parent)

file_protocol=pwd+"/fixtures/protocol.csv"
file_dbdata=pwd+"/fixtures/dbdata.csv"
file_dbtype=pwd+"/fixtures/dbtype.csv"
file_station=pwd+"/fixtures/station.csv"

session=SessionCollector()

this_protocol=dict()

with open(file_protocol, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        p=row['name']
        if not session.get_protocol_id(p) and p !='':
            this_protocol[int(row['id'])]=session.protocol(**row)

this_dbtype=dict()

print("Protocol ok")

with open(file_dbtype, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        p=row['name']
        if not session.get_dbtype_id(p) and p !='':
            this_dbtype[int(row['id'])]=session.dbtype(**row)

this_dbdata=dict()

print("DBType ok")

with open(file_dbdata, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        if not row['port'].isdigit():
            row['port']=0
        p=row['code']
        if not session.get_dbdata_id(p) and p !='':
            this_dbdata[int(row['id'])]=session.dbdata(**row)

this_station=dict()

print("DBdata ok")

with open(file_station, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        port=row['port']
        iport=row['interface_port']
        if not row['port'].isdigit():
            port=int(0)
        if not row['interface_port'].isdigit():
            iport=int(0)
            print("Interface port %d" % iport)
        p=row['code']
        if not session.get_station_id(p) and p !='':
            station_data=dict(
                code=row['code'],
                name=row['name'],
                position=[float(row['lat']), float(row['lon'])],
                host=row['host'],
                port=port,
                interface_port=iport,
                db=row['db'],
                protocol=row['protocol']
            )
            this_station[int(row['id'])]=session.station(**station_data)

print("Station ok")
