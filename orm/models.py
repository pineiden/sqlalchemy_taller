from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy import Column, Integer, Text, String, DateTime, ForeignKey

# GEO
from geoalchemy2 import Geography
from geoalchemy2 import functions as geo_func

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from geoalchemy2.shape import to_shape

#from osgeo import ogr

try:
    from .validates import isIp, isURL
except Exception:
    from validates import isIp, isURL

from osgeo import ogr
from base64 import b64decode

Base = declarative_base()


class Protocol(Base):
    """
    Define protocols for data communication
    """

    __tablename__ = 'protocol'
    __table_args__ = {'schema': 'collector'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(15), unique=True)
    ref_url = Column(String(300), unique=True, nullable=True)
    class_name = Column(String(20), unique=False, nullable=True)
    git_url = Column(String(300), unique=False, nullable=True)

    stations_prot = relationship('Station', backref='protocol')

    @validates('ref_url', 'git_url')
    def validate_url(self, key, value):
        if isURL(value):
            return value
        else:
            None


class DBType(Base):
    """
    Define database information for store data
    """

    __tablename__ = 'dbtype'
    __table_args__ = {'schema': 'collector'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    typedb = Column(String(12), unique=False)
    # name software
    name = Column(String(12), unique=True, nullable=True)
    url = Column(String(300), unique=True, nullable=True)
    data_list = Column(String(300), unique=False, nullable=True)

    dbdatas = relationship('DBData', backref='dbtype')

    @validates('typedb')
    def validate_typedb(self, key, value):
        if value.upper() in ['TEXT', 'SQL', 'NO-SQL']:
            return value.upper()
        else:
            return 'TEXT'


class DBData(Base):
    """
    Define database information for store data
    """

    __tablename__ = 'dbdata'
    __table_args__ = {'schema': 'collector'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    # ascii, sql, no-sql
    # name software
    # code for this database en particular
    code = Column(String(12), unique=True)
    path = Column(String(200), unique=True, nullable=True)
    host = Column(String(25), unique=False, nullable=True)
    port = Column(Integer, unique=False, nullable=True)
    user = Column(String(25), unique=False, nullable=True)
    passw = Column(String(200), unique=False, nullable=True)
    info = Column(String(255), unique=False, nullable=True)
    dbtype_id = Column(Integer, ForeignKey('collector.dbtype.id'))

    stations_db = relationship('Station', backref='dbdata')

    @validates('host')
    def validate_host(self, key, host):
        rev = isIp(host)
        if rev:
            return host
        else:
            return 'localhost'

class Station(Base):
    """
    Define basic station information
    """

    __tablename__ = 'station'
    __table_args__ = {'schema': 'collector'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String(8), unique=True)
    name = Column(String(40), unique=False, nullable=True)
    position = Column(Geography(geometry_type='POINT', srid=4326))
    port = Column(Integer, unique=False, default=0)
    interface_port = Column(Integer, unique=False, default=0)
    host = Column(String(80), unique=False)  # validate ip

    db_id = Column(Integer, ForeignKey('collector.dbdata.id'))
    protocol_id = Column(Integer, ForeignKey('collector.protocol.id'))


    @validates('port')
    def validate_port(self, key, port):
        assert port >= 0 and port < 65500, "No es un valor correcto"
        return port

    @validates('interface_port')
    def validate_port(self, key, interface_port):
        print("El puerto de interface se valida")
        print(interface_port)
        if interface_port==None:
            interface_port=0
        assert interface_port >= 0 and interface_port < 65500, "No es un valor correcto"
        return interface_port

    @validates('host')
    def validate_host(self, key, host):
        url = ''
        print("Type %s" % format(type(self.interface_port)))
        print("Interface port : %s" % format(self.interface_port) )
        #if self.interface_port > 0:
        #    url = "http://" + str(host) + ":" + str(self.interface_port)
        #else:
        url = "http://" + str(host)
        print(url)
        print(host)
        assert isIp(host) or isURL(url)
        return host

    @property
    def interface_url(self):
        print(self.interface_port)
        print(self.host)
        if self.interface_port > 0:
            return "http://" + str(self.host) + ":" + str(self.interface_port)
        else:
            return "http://" + str(self.host)

    def get_position(self):
        point = to_shape(self.position)
        return point #list(point.coords)

