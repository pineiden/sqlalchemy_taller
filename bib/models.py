from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy import Column, Integer, Text, String, \
    DateTime, ForeignKey, Boolean


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from base64 import b64decode

from rutchile.rut import RutChile

import datetime

from sqlalchemy.orm import validates

Base = declarative_base()

class Familia(Base):
    __tablename__ = 'familia'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)  
    nombre = Column(String(15), unique=True)
    
    categoria_fam = relationship('Categoria', backref='familia')

class Categoria(Base):

    __tablename__ = 'categoria'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    nombre = Column(String(15), unique=True, nullable=True)
    descripcion= Column(String(300), unique=False, nullable=True)

    familia_id=Column(Integer, ForeignKey('biblioteca.familia.id'))

    titulo_cat = relationship('Titulo', backref='categoria')

 
class Autor(Base):
    __tablename__ = 'autor'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)      
    nombre=Column(String(200), unique=False)
    nacionalidad=Column(String(20), unique=True)
    fecha_nac=Column(DateTime(), unique=False, nullable=True)

    titulo_aut = relationship('Titulo', backref='autor')


class Titulo(Base):
    __tablename__ = 'titulo'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)

    nombre=Column(String(200), unique=False)
    subtitulo=Column(String(200), unique=False, nullable=True)
    paginas=Column(Integer, unique=False, nullable=True)
    isbn=Column(String(200), unique=True, nullable=True)
    codigo=Column(String(200), unique=True, nullable=True)

    autor_id=Column(Integer, ForeignKey('biblioteca.autor.id'))
    categoria_id=Column(Integer, ForeignKey('biblioteca.categoria.id'))

    copia_titulo = relationship('Copia', backref='titulo')


class Copia(Base):
    __tablename__ = 'copia'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    estado=Column(Boolean)
    codigo_copia=Column(String(20), unique=True, nullable=False)

    titulo_id=Column(Integer, ForeignKey('biblioteca.titulo.id'))
    
    prestamo_copia = relationship('Prestamo', backref='copia')


class ClasePersona(Base):
    __tablename__ = 'clase_persona'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)

    persona_clase = relationship('Persona', backref='clase_persona')

class Persona(Base):

    __tablename__ = 'persona'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    nombre=Column(String(200), unique=False)
    rut=Column(String(20), unique=True)
    fono=Column(String(20), unique=False)
    email=Column(String(30), unique=True, nullable=False)

    clase_persona_id=Column(Integer, ForeignKey('biblioteca.clase_persona.id'))

    prestamo_persona = relationship('Prestamo', backref='persona')

    @validates('email')
    def validate_email(self, key, address):
        assert '@' in address
        return address

    @validates('rut')
    def validate_rut(self, key, rut):
        assert RutChile(rut).es_rut
        return rut

class Prestamo(Base):
    __tablename__ = 'protocol'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    fecha=Column(DateTime, default=datetime.datetime.utcnow)

    id_lector=Column(Integer, ForeignKey('biblioteca.titulo.id'))
    id_bibliotecario=Column(Integer, ForeignKey('biblioteca.titulo.id'))
    id_copia=Column(Integer, ForeignKey('biblioteca.titulo.id'))

