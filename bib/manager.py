from sqlalchemy import update
from sqlalchemy import Table, MetaData
from sqlalchemy.sql import text
from sqlalchemy.sql import select
from sqlalchemy.sql import table
from sqlalchemy.sql import column
from sqlalchemy import inspect

import datetime


try:
    from .db_session import session, engine, connection
except Exception:
    from db_session import session, engine, connection


try:
    from .models import Familia, Categoria, Autor, Titulo, Copia, ClasePersona, Persona, Prestamo
except Exception:
    from models import  Familia, Categoria, Autor, Titulo, Copia, ClasePersona, Persona, Prestamo


class SessionHandle(object):
    """
    A session middleware class to manage the basic elements in the database,
    has generic methods to verify the elements existence, update_ and obtain
    lists from tables
    """

    def __init__(self):
        self.session = session
        self.conn = connection
        self.metadata = MetaData()

    def close(self):
        self.session.close()

    def exists_table(self, table_name):
        """
        Check if table_name exists on schema
        :param table_name: a table_name string
        :return: boolean {True,False}
        """
        return engine.dialect.has_table(engine.connect(), table_name)

    def exists_field(self, table_name, field_name):
        """
        Check if field exist in table
        :param table_name: table name string
        :param field_name: field name string
        :return:  bolean {True, False}
        """
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        try:
            r.remove('id')
        except ValueError:
            pass
        assert field_name in r, 'No existe este campo en tabla'
        return True

    def value_type(self, table_name, field_name, value):
        """
        Check if value is the same value type in field
        :param table_name: table name string
        :param field_name: field name string
        :param value:  some value
        :return: boolean {True, False, None}; None if value type doesn't exist
        on that list, because there are only the most common types.
        """
        # get type value
        assert self.exists_table(table_name), 'No existe esta tabla'
        fields = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        r = [c.name for c in fields.columns]
        assert self.exists_field(table_name, field_name), \
            'Campo no existe en tabla'
        this_index = r.index(field_name)
        t = [str(c.type) for c in fields.columns]
        this_type = t[this_index]
        b = False
        if this_type == 'INTEGER' or this_type == 'BigInteger':
            assert isinstance(value, int)
            b = True
        elif this_type[0:7] == 'VARCHAR' or this_type == 'TEXT' or this_type == 'STRING':
            assert isinstance(value, str)
            b = True
        elif this_type == 'BOOLEAN':
            assert isinstance(value, bool)
            b = True
        elif this_type == 'DATE':
            assert isinstance(value, datetime.date)
            b = True
        elif this_type == 'DATETIME':
            assert isinstance(value, datetime.datetime)
            b = True
        elif this_type == 'FLOAT' or this_type == 'NUMERIC':
            assert isinstance(value, float)
            b = True
        else:
            b = None

        return b
        # check

        def generic_query(self, st):
            q = text(st)
            u = self.session.execute(q)
            return(u)

    def update_table(self, table_name, instance, field_name, value):
        """
        Change some value in table
        :param table_name: table name class
        :param instance: instance to modify in database
        :param field: field name string
        :param value: value
        :return: void()
        """
        # update to database
        table = Table(
            table_name, self.metadata, autoload=True, autoload_with=engine)
        # print(table+":"+field_name+":"+value)
        up = update(table).where(
            table.c.id == instance.id).values({field_name: value})
        print(up)
        self.session.execute(up)
        self.session.commit()

    # LIST ELEMENTS

    def get_list(self, model):
        """
        Get the complete list of elements in some Model Class (Table in db)

        :param model:Model Class
        :return: a query list
        """
        return self.session.query(model).all()

# Class Alias
SH = SessionHandle

class SessionBiblioteca(SH):
    """
    Un manager de sesión especifico para los modelos
    """
    def familia(self):
        pass

    def create_familia(self):
        pass

    def categoria(self):
        pass

    def create_categoria(self):
        pass

    def autor(self):
        pass

    def create_author(self):
        pass

    def titulo(self):
        pass
        
    def creat_titulo(self):
        pass
        
    def copia(self):
        pass
    
    def create_copia(self):
        pass
        
    def clase_persona(self):
        pass
        
    def create_clase_persona(self):
        pass
        
    def persona(self):
        pass
        
    def create_persona(self):
        pass
        
    def prestamo(self):
        pass
        
    def create_prestamo(self):
        pass

