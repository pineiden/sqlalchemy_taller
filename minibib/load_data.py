from manager import SessionBiblioteca
from pathlib import Path
import csv

pwd = str(Path(__file__).resolve().parent)

file_libro=pwd+"/fixtures/libro.csv"
file_prestamos=pwd+"/fixtures/prestamos.csv"
file_usuario=pwd+"/fixtures/usuario.csv"

session=SessionBiblioteca()

this_libro=dict()

with open(file_libro, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        p=row['codigo']
        if not session.get_titulo_id(p) and p !='':
            this_libro[int(row['id'])]=session.titulo(**row)

this_usuario=dict()

print("Libros ok")

with open(file_usuario, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        p=row['rut']
        if not session.get_persona_id(p) and p !='':
            this_usuario[int(row['id'])]=session.persona(**row)

this_prestamo=dict()

print("Usuarios ok")

with open(file_prestamos, 'r') as rfile:
    reader = csv.DictReader(rfile, delimiter=';', quoting=csv.QUOTE_NONE)
    for row in reader:
        print(row)
        this_prestamo[int(row['id'])]=session.prestamo(**row)
            
print("Prestamos ok")

