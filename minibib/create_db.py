from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

from models import Base


user = 'escritor'
passw = 'versos+tristes'
dbname = 'mini_biblioteca'
db_engine = "postgresql://{}:{}@localhost/{}".format(user, passw, dbname)
#create engine

engine = create_engine(db_engine, echo=True)

try:
    engine.execute(CreateSchema('biblioteca'))
except Exception as exec:
    print(exec)
    raise exec
#load schema on engine
print("Create schema biblioteca")
try:
    Base.metadata.create_all(engine, checkfirst=True)
except Exception as exec:
    print(exec)
    raise exec
