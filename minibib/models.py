from sqlalchemy.orm import relationship
from sqlalchemy.orm import validates
from sqlalchemy import UniqueConstraint, Index
from sqlalchemy import Column, Integer, Text, String, \
    DateTime, ForeignKey, Boolean


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from base64 import b64decode

from rutchile.rut import RutChile

import datetime

from sqlalchemy.orm import validates

Base = declarative_base()


class Titulo(Base):
    __tablename__ = 'titulo'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)

    nombre=Column(String(200), unique=False)
    paginas=Column(Integer, unique=False, nullable=True)
    codigo=Column(String(200), unique=True, nullable=True)
    autor=Column(String(200), unique=True, nullable=True)

    prestamo_titulo = relationship('Prestamo', backref='titulo')


class Persona(Base):

    __tablename__ = 'persona'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    nombre=Column(String(200), unique=False)
    rut=Column(String(20), unique=True)
    fono=Column(String(20), unique=False)
    email=Column(String(30), unique=True, nullable=False)

    prestamo_persona = relationship('Prestamo', backref='persona')

    @validates('email')
    def validate_email(self, key, address):
        assert '@' in address
        return address

    @validates('rut')
    def validate_rut(self, key, rut):
        assert RutChile(rut).es_rut
        return rut

class Prestamo(Base):
    __tablename__ = 'prestamo'
    __table_args__ = {'schema': 'biblioteca'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    fecha=Column(DateTime, default=datetime.datetime.utcnow)

    lector_id=Column(Integer, ForeignKey('biblioteca.persona.id'))
    titulo_id=Column(Integer, ForeignKey('biblioteca.titulo.id'))

